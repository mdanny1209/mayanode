#!/bin/sh

# /kujirad init --chain-id harpoon-4 local
# /kujirad add-genesis-account kujira1qr79m0r8hzj0t88c3c6k99prxv0fe34ulfzyzk 100000000ukuji  # smoke contrib
# /kujirad add-genesis-account kujira1y4lj8cg47kfm70nht5f8ajyvr4dftfc6lmvga7 100000000ukuji  # smoke master

mkdir -p /root/.kujira/data
cat >/root/.kujira/data/priv_validator_state.json <<EOF
{
  "height": "0",
  "round": 0,
  "step": 0
}
EOF

exec /entrypoint.sh
