package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/mayachain/mayanode/config"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
	"gitlab.com/thorchain/tss/go-tss/tss"
)

// -------------------------------------------------------------------------------------
// Responses
// -------------------------------------------------------------------------------------

type P2PStatusPeer struct {
	Address string `json:"address"`
	IP      string `json:"ip"`
	Status  string `json:"status"`

	StoredPeerID   string `json:"stored_peer_id"`
	NodesPeerID    string `json:"nodes_peer_id"`
	ReturnedPeerID string `json:"returned_peer_id"`

	P2PPortOpen bool  `json:"p2p_port_open"`
	P2PDialMs   int64 `json:"p2p_dial_ms"`
}

type P2PStatusResponse struct {
	MayanodeHeight int64           `json:"mayanode_height"`
	Peers          []P2PStatusPeer `json:"peers"`
	PeerCount      int             `json:"peer_count"`
	Errors         []string        `json:"errors"`
}

// -------------------------------------------------------------------------------------
// Health Server
// -------------------------------------------------------------------------------------

// HealthServer to provide something for health check and also p2pid
type HealthServer struct {
	logger    zerolog.Logger
	s         *http.Server
	tssServer tss.Server
}

// NewHealthServer create a new instance of health server
func NewHealthServer(addr string, tssServer tss.Server) *HealthServer {
	hs := &HealthServer{
		logger:    log.With().Str("module", "http").Logger(),
		tssServer: tssServer,
	}
	s := &http.Server{
		Addr:              addr,
		Handler:           hs.newHandler(),
		ReadHeaderTimeout: 2 * time.Second,
	}
	hs.s = s

	return hs
}

func (s *HealthServer) newHandler() http.Handler {
	router := mux.NewRouter()
	router.Handle("/ping", http.HandlerFunc(s.pingHandler)).Methods(http.MethodGet)
	router.Handle("/p2pid", http.HandlerFunc(s.getP2pIDHandler)).Methods(http.MethodGet)
	router.Handle("/status/p2p", http.HandlerFunc(s.p2pStatus)).Methods(http.MethodGet)
	return router
}

func (s *HealthServer) pingHandler(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (s *HealthServer) getP2pIDHandler(w http.ResponseWriter, _ *http.Request) {
	localPeerID := s.tssServer.GetLocalPeerID()
	_, err := w.Write([]byte(localPeerID))
	if err != nil {
		s.logger.Error().Err(err).Msg("fail to write to response")
	}
}

func (s *HealthServer) p2pStatus(w http.ResponseWriter, _ *http.Request) {
	res := &P2PStatusResponse{Peers: make([]P2PStatusPeer, 0)}

	// get thorchain nodes
	nodesByIP := map[string]types.QueryNodeAccount{}
	mayanode := config.GetBifrost().MayaChain.ChainHost
	url := fmt.Sprintf("http://%s/mayachain/nodes", mayanode)
	// trunk-ignore(golangci-lint/gosec)
	resp, err := http.Get(url)
	if err != nil {
		s.logger.Error().Err(err).Msg("fail to get mayanode status")
	} else {
		defer resp.Body.Close()

		// set the height from header
		res.MayanodeHeight, err = strconv.ParseInt(resp.Header.Get("x-mayachain-height"), 10, 64)
		if err != nil {
			s.logger.Error().Err(err).Msg("fail to parse mayanode height")
		}

		nodes := make([]types.QueryNodeAccount, 0)
		if err := json.NewDecoder(resp.Body).Decode(&nodes); err != nil {
			s.logger.Error().Err(err).Msg("fail to decode mayanode status")
		} else {
			for _, node := range nodes {
				otherNode, exists := nodesByIP[node.IPAddress]

				if !exists || (otherNode.Status != types.NodeStatus_Active && otherNode.PreflightStatus.Status != types.NodeStatus_Ready) {
					// only add node if the IP is not already in the map
					nodesByIP[node.IPAddress] = node
				} else if node.Status == types.NodeStatus_Active || node.PreflightStatus.Status == types.NodeStatus_Ready {
					// if both nodes are active or ready, report an error
					res.Errors = append(res.Errors, fmt.Sprintf("active node IP reuse: %s", node.IPAddress))
				}
			}
		}
	}

	// get all connected peers
	peerInfos := s.tssServer.GetKnownPeers()
	res.PeerCount = len(peerInfos)

	// ping and http get /p2pid on all peers
	mu := sync.Mutex{}
	wg := sync.WaitGroup{}
	for _, pi := range peerInfos {
		wg.Add(1)
		go func(pi tss.PeerInfo) {
			peer := P2PStatusPeer{
				StoredPeerID: pi.ID,
				IP:           pi.Address,
			}

			defer func() {
				mu.Lock()
				res.Peers = append(res.Peers, peer)
				mu.Unlock()
				wg.Done()
			}()

			// nothing to do if no addresses
			if pi.Address == "" {
				return
			}

			// check if the node is in thornode
			if node, ok := nodesByIP[pi.Address]; ok {
				peer.Address = node.NodeAddress.String()
				peer.Status = node.Status.String()
				peer.NodesPeerID = node.PeerID
			}

			// get the peer id
			resp, err := http.Get(fmt.Sprintf("http://%s:6040/p2pid", pi.Address))
			status := ""
			if resp != nil {
				status = resp.Status
			}
			if err != nil {
				peer.ReturnedPeerID = fmt.Sprintf("failed, status=\"%s\"", status)
			} else {
				defer resp.Body.Close()
				b, err := io.ReadAll(resp.Body)
				if err != nil {
					peer.ReturnedPeerID = fmt.Sprintf("failed to read body, status=\"%s\"", status)
				} else {
					peer.ReturnedPeerID = string(b)
				}
			}

			// check the p2p port
			start := time.Now()
			peer.P2PPortOpen = checkPortOpen(pi.Address, 5040)
			peer.P2PDialMs = int64(time.Since(start) / time.Millisecond)
		}(pi)
	}
	wg.Wait()

	// write the response
	jsonBytes, err := json.MarshalIndent(res, "", "  ")
	if err != nil {
		s.logger.Error().Err(err).Msg("fail to write to response")
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		_, err = w.Write(jsonBytes)
		if err != nil {
			s.logger.Error().Err(err).Msg("fail to write to response")
		}
	}
}

// Start health server
func (s *HealthServer) Start() error {
	if s.s == nil {
		return errors.New("invalid http server instance")
	}
	if err := s.s.ListenAndServe(); err != nil {
		if err != http.ErrServerClosed {
			return fmt.Errorf("fail to start http server: %w", err)
		}
	}
	return nil
}

func (s *HealthServer) Stop() error {
	c, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := s.s.Shutdown(c)
	if err != nil {
		log.Error().Err(err).Msg("Failed to shutdown the Tss server gracefully")
	}
	return err
}

func checkPortOpen(host string, port int) bool {
	address := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.DialTimeout("tcp", address, 2*time.Second)
	if err != nil {
		return false
	}
	defer conn.Close()
	return true
}
